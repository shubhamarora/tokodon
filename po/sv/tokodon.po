# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the tokodon package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: tokodon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-06 00:47+0000\n"
"PO-Revision-Date: 2022-05-21 11:59+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: account/abstractaccount.cpp:430
#, kde-format
msgid "Could not follow account"
msgstr "Kunde inte följa konto"

#: account/abstractaccount.cpp:431
#, kde-format
msgid "Could not unfollow account"
msgstr "Kunde inte sluta följa konto"

#: account/abstractaccount.cpp:432
#, kde-format
msgid "Could not block account"
msgstr "Kunde inte blockera konto"

#: account/abstractaccount.cpp:433
#, kde-format
msgid "Could not unblock account"
msgstr "Kunde inte sluta blockera konto"

#: account/abstractaccount.cpp:434
#, kde-format
msgid "Could not mute account"
msgstr "Kunde inte tysta konto"

#: account/abstractaccount.cpp:435
#, kde-format
msgid "Could not unmute account"
msgstr "Kunde inte sluta tysta konto"

#: account/abstractaccount.cpp:436
#, kde-format
msgid "Could not feature account"
msgstr "Kunde inte presentera konto"

#: account/abstractaccount.cpp:437
#, kde-format
msgid "Could not unfeature account"
msgstr "Kunde inte sluta presentera konto"

#: account/abstractaccount.cpp:438
#, kde-format
msgid "Could not edit note about an account"
msgstr "Kunde inte redigera anmärkning om ett konto"

#: account/notificationhandler.cpp:23
#: content/ui/StatusDelegate/StatusDelegate.qml:159
#, kde-format
msgid "%1 favorited your post"
msgstr "%1 markerade ditt inlägg som favorit"

#: account/notificationhandler.cpp:27 content/ui/FollowDelegate.qml:44
#: content/ui/FollowDelegate.qml:59
#, kde-format
msgid "%1 followed you"
msgstr "%1 följer dig"

#: account/notificationhandler.cpp:31
#: content/ui/StatusDelegate/StatusDelegate.qml:258
#, kde-format
msgid "%1 boosted your post"
msgstr "%1 förstärkte ditt inlägg"

#: account/notificationhandler.cpp:39
#: content/ui/StatusDelegate/StatusDelegate.qml:161
#, fuzzy, kde-format
#| msgid "%1 favorited your post"
msgid "%1 edited a post"
msgstr "%1 markerade ditt inlägg som favorit"

#: account/profileeditor.cpp:183
#, kde-format
msgid "Image is too big"
msgstr ""

#: account/profileeditor.cpp:196
#, kde-format
msgid "Unsupported image file. Only jpeg, png and gif are supported."
msgstr ""

#: account/profileeditor.cpp:323
#, kde-format
msgid "Account details saved"
msgstr ""

#: account/socialgraphmodel.cpp:30
#, fuzzy, kde-format
#| msgid "Follow"
msgctxt "@title"
msgid "Follow Requests"
msgstr "Följ"

#: account/socialgraphmodel.cpp:32
#, fuzzy, kde-format
#| msgid "Follow"
msgctxt "@title"
msgid "Followers"
msgstr "Följ"

#: account/socialgraphmodel.cpp:34
#, fuzzy, kde-format
#| msgid "Following"
msgctxt "@title"
msgid "Following"
msgstr "Följer"

#: content/ui/AccountInfo.qml:70
#, fuzzy, kde-format
#| msgid "Follow"
msgid "Follows you"
msgstr "Följ"

#: content/ui/AccountInfo.qml:110
#, kde-format
msgid "Requested"
msgstr "Begärt"

#: content/ui/AccountInfo.qml:113
#, kde-format
msgid "Following"
msgstr "Följer"

#: content/ui/AccountInfo.qml:115
#, kde-format
msgid "Follow"
msgstr "Följ"

#: content/ui/AccountInfo.qml:140
#, kde-format
msgid "Stop notifying me when %1 posts"
msgstr "Sluta underrätta mig när %1 skickar ett inlägg"

#: content/ui/AccountInfo.qml:142
#, kde-format
msgid "Notify me when %1 posts"
msgstr "Underrätta mig när %1 skickar ett inlägg"

#: content/ui/AccountInfo.qml:159
#, fuzzy, kde-format
#| msgid "Hide boosts from %1"
msgid "Hide Boosts from %1"
msgstr "Dölj förstärkningar från %1"

#: content/ui/AccountInfo.qml:161
#, fuzzy, kde-format
#| msgid "Stop hiding boosts from %1"
msgid "Stop Hiding Boosts from %1"
msgstr "Sluta dölja förstärkningar från %1"

#: content/ui/AccountInfo.qml:178
#, fuzzy, kde-format
#| msgid "Stop featuring on profile"
msgid "Stop Featuring on Profile"
msgstr "Sluta presentera för profil"

#: content/ui/AccountInfo.qml:180
#, fuzzy, kde-format
#| msgid "Feature on profile"
msgid "Feature on Profile"
msgstr "Presentera för profil"

#: content/ui/AccountInfo.qml:197
#, fuzzy, kde-format
#| msgid "Stop muting"
msgid "Stop Muting"
msgstr "Sluta tysta"

#: content/ui/AccountInfo.qml:199
#, kde-format
msgid "Mute"
msgstr "Tysta"

#: content/ui/AccountInfo.qml:216
#, fuzzy, kde-format
#| msgid "Stop blocking"
msgid "Stop Blocking"
msgstr "Sluta blockera"

#: content/ui/AccountInfo.qml:218
#, kde-format
msgid "Block"
msgstr "Blockera"

#: content/ui/AccountInfo.qml:233
#, fuzzy, kde-format
#| msgid "Feature on profile"
msgid "Edit Profile"
msgstr "Presentera för profil"

#: content/ui/AccountInfo.qml:237 content/ui/Settings/AccountsCard.qml:36
#, fuzzy, kde-format
#| msgid "Accounts"
msgid "Account editor"
msgstr "Konton"

#: content/ui/AccountInfo.qml:244 content/ui/TimelinePage.qml:130
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgid "Settings"
msgstr "Inställningar"

#: content/ui/AccountInfo.qml:245 content/ui/TimelinePage.qml:132
#: content/ui/UserInfo.qml:218
#, kde-format
msgid "Configure"
msgstr ""

#: content/ui/AccountInfo.qml:251 content/ui/main.qml:211
#, fuzzy, kde-format
#| msgid "Follow"
msgid "Follow Requests"
msgstr "Följ"

#: content/ui/AccountInfo.qml:328
#, kde-format
msgid "Note"
msgstr ""

#: content/ui/AccountInfo.qml:336
#, kde-format
msgid "Saved"
msgstr ""

#: content/ui/AccountInfo.qml:351
#, kde-format
msgid "Click to add a note"
msgstr "Klicka för att lägga till en anmärkning"

#: content/ui/AccountInfo.qml:427
#, kde-format
msgctxt "@label User's number of statuses"
msgid "<b>%1</b> posts"
msgstr ""

#: content/ui/AccountInfo.qml:439
#, fuzzy, kde-format
#| msgid "%1 followers"
msgctxt "@label User's number of followers"
msgid "<b>%1</b> followers"
msgstr "%1 följare"

#: content/ui/AccountInfo.qml:451
#, fuzzy, kde-format
#| msgid "%1 following"
msgctxt "@label User's number of followed accounts"
msgid "<b>%1</b> following"
msgstr "%1 följer"

#: content/ui/AccountInfo.qml:463
#, fuzzy, kde-format
#| msgctxt "Share a post"
#| msgid "Boost"
msgctxt "@item:inmenu Profile Post Filter"
msgid "Posts"
msgstr "Förstärk"

#: content/ui/AccountInfo.qml:467
#, kde-format
msgctxt "@item:inmenu Profile Post Filter"
msgid "Posts && Replies"
msgstr ""

#: content/ui/AccountInfo.qml:471
#, kde-format
msgctxt "@item:inmenu Profile Post Filter"
msgid "Media"
msgstr ""

#: content/ui/AccountInfo.qml:498
#, fuzzy, kde-format
#| msgid "Hide boosts from %1"
msgctxt "@option:check"
msgid "Hide boosts"
msgstr "Dölj förstärkningar från %1"

#: content/ui/AuthorizationPage.qml:15
#, kde-format
msgid "Authorization"
msgstr ""

#: content/ui/AuthorizationPage.qml:29
#, kde-format
msgid "Authorize Tokodon to act on your behalf"
msgstr ""

#: content/ui/AuthorizationPage.qml:41
#, kde-format
msgid "To continue, please open the following link and authorize Tokodon: %1"
msgstr "Fortsätt genom att öppna följande länk och godkänna Tokodon:%1"

#: content/ui/AuthorizationPage.qml:57 content/ui/AuthorizationPage.qml:76
#, fuzzy, kde-format
#| msgid "Open link"
msgid "Open Link"
msgstr "Öppna länk"

#: content/ui/AuthorizationPage.qml:62 content/ui/AuthorizationPage.qml:84
#, fuzzy, kde-format
#| msgid "Copy link"
msgid "Copy Link"
msgstr "Kopiera länk"

#: content/ui/AuthorizationPage.qml:65 content/ui/AuthorizationPage.qml:87
#, kde-format
msgid "Link copied."
msgstr ""

#: content/ui/AuthorizationPage.qml:101
#, kde-format
msgid "Enter token:"
msgstr "Ange token:"

#: content/ui/AuthorizationPage.qml:109 content/ui/LoginPage.qml:49
#, kde-format
msgid "Continue"
msgstr "Fortsätt"

#: content/ui/AuthorizationPage.qml:112
#, kde-format
msgid "Please insert the generated token."
msgstr "Infoga genererade symbol."

#: content/ui/ConversationPage.qml:11
#, kde-format
msgid "Conversations"
msgstr ""

#: content/ui/ConversationPage.qml:35
#, fuzzy, kde-format
#| msgctxt "Show only mentions"
#| msgid "Mentions"
msgid "No Conversations"
msgstr "Omnämnanden"

#: content/ui/ExplorePage.qml:15 content/ui/main.qml:304
#, kde-format
msgid "Explore"
msgstr ""

#: content/ui/ExplorePage.qml:30 content/ui/NotificationPage.qml:30
#: content/ui/TimelinePage.qml:44 search/searchmodel.cpp:141
#, kde-format
msgid "Post"
msgstr ""

#: content/ui/ExplorePage.qml:84
#, fuzzy, kde-format
#| msgctxt "Share a post"
#| msgid "Boost"
msgid "No Posts"
msgstr "Förstärk"

#: content/ui/LanguageSelector.qml:64
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgctxt "@item:inlistbox Group of preferred languages"
msgid "Preferred Languages"
msgstr "Valt standardspråk:"

#: content/ui/LanguageSelector.qml:65
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgctxt "@item:inlistbox Group of all languages"
msgid "All Languages"
msgstr "Valt standardspråk:"

#: content/ui/LoginPage.qml:14
#, kde-format
msgid "Login"
msgstr "Logga in"

#: content/ui/LoginPage.qml:22 content/ui/TimelinePage.qml:66
#, kde-format
msgctxt "@info:status Network status"
msgid "Failed to contact server: %1. Please check your settings."
msgstr ""

#: content/ui/LoginPage.qml:35
#, kde-format
msgid "Welcome to Tokodon"
msgstr "Välkommen till Tokodon"

#: content/ui/LoginPage.qml:40
#, kde-format
msgid "Instance Url:"
msgstr "Instans Url:"

#: content/ui/LoginPage.qml:52
#, fuzzy, kde-format
#| msgid "Instance URL and username must not be empty!"
msgid "Instance URL must not be empty!"
msgstr "Instansens webbadress och användarnamn får inte vara tomma."

#: content/ui/LoginPage.qml:76
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgctxt "@title:group Login page"
msgid "Network Settings"
msgstr "Inställningar"

#: content/ui/LoginPage.qml:81
#, fuzzy, kde-format
#| msgid "Ignore ssl errors"
msgctxt "@option:check Login page"
msgid "Ignore SSL errors"
msgstr "Ignorera ssl-fel"

#: content/ui/LoginPage.qml:88 content/ui/Settings/NetworkProxyPage.qml:72
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgid "Proxy Settings"
msgstr "Inställningar"

#: content/ui/main.qml:182
#, kde-format
msgid "Home"
msgstr "Hem"

#: content/ui/main.qml:198 content/ui/NotificationPage.qml:15
#, kde-format
msgid "Notifications"
msgstr "Underrättelser"

#: content/ui/main.qml:227
#, kde-format
msgid "Local"
msgstr "Lokal"

#: content/ui/main.qml:242
#, kde-format
msgid "Global"
msgstr "Global"

#: content/ui/main.qml:258
#, fuzzy, kde-format
#| msgctxt "Show only mentions"
#| msgid "Mentions"
msgid "Conversation"
msgstr "Omnämnanden"

#: content/ui/main.qml:272
#, kde-format
msgid "Favourites"
msgstr ""

#: content/ui/main.qml:288
#, kde-format
msgid "Bookmarks"
msgstr ""

#: content/ui/main.qml:318
#, kde-format
msgid "Search"
msgstr ""

#: content/ui/NotificationPage.qml:37
#, kde-format
msgctxt "Show all notifications"
msgid "All"
msgstr "Alla"

#: content/ui/NotificationPage.qml:48
#, kde-format
msgctxt "Show only mentions"
msgid "Mentions"
msgstr "Omnämnanden"

#: content/ui/NotificationPage.qml:57
#, kde-format
msgctxt "Show only favorites"
msgid "Favorites"
msgstr ""

#: content/ui/NotificationPage.qml:66
#, fuzzy, kde-format
#| msgctxt "Share a post"
#| msgid "Boost"
msgctxt "Show only boosts"
msgid "Boosts"
msgstr "Förstärk"

#: content/ui/NotificationPage.qml:75
#, kde-format
msgctxt "Show only poll results"
msgid "Poll results"
msgstr ""

#: content/ui/NotificationPage.qml:84
#, fuzzy, kde-format
#| msgid "Follow"
msgctxt "Show only follows"
msgid "Follows"
msgstr "Följ"

#: content/ui/NotificationPage.qml:196
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "No Notifications"
msgstr "Underrättelser"

#: content/ui/SearchPage.qml:16
#, kde-format
msgctxt "@title"
msgid "Search"
msgstr ""

#: content/ui/SearchView.qml:33
#, fuzzy, kde-format
#| msgid "Loading"
msgid "Loading..."
msgstr "Läser in"

#: content/ui/SearchView.qml:41
#, kde-format
msgid "No search results"
msgstr ""

#: content/ui/SearchView.qml:49
#, kde-format
msgid "Search for peoples, tags and posts"
msgstr ""

#: content/ui/Settings/AccountsCard.qml:24
#, kde-format
msgid "Accounts"
msgstr "Konton"

#: content/ui/Settings/AccountsCard.qml:72
#, kde-format
msgid "Logout"
msgstr "Logga ut"

#: content/ui/Settings/AccountsCard.qml:92 content/ui/UserInfo.qml:69
#, kde-format
msgid "Add Account"
msgstr "Lägg till konto"

#: content/ui/Settings/GeneralCard.qml:19
#, kde-format
msgid "General"
msgstr "Allmänt"

#: content/ui/Settings/GeneralCard.qml:24
#, fuzzy, kde-format
#| msgid "Show detailed statistics about posts."
msgid "Show detailed statistics about posts"
msgstr "Visa detaljerad statistik om inlägg."

#: content/ui/Settings/GeneralCard.qml:37
#, fuzzy, kde-format
#| msgid "Show link preview."
msgid "Show link preview"
msgstr "Visa förhandsgranskning av länk."

#: content/ui/Settings/GeneralCard.qml:50
#, kde-format
msgid "Crop images in the timeline to 16:9"
msgstr ""

#: content/ui/Settings/GeneralCard.qml:62
#, kde-format
msgid "Auto-play animated GIFs"
msgstr ""

#: content/ui/Settings/GeneralCard.qml:75
#, kde-format
msgid "Color theme"
msgstr ""

#: content/ui/Settings/GeneralCard.qml:92
#, fuzzy, kde-format
#| msgid "Content Warning"
msgid "Content font"
msgstr "Innehållsvarning"

#: content/ui/Settings/GeneralCard.qml:98
#, fuzzy, kde-format
#| msgid "Please choose a file"
msgid "Please choose a font"
msgstr "Välj en fil"

#: content/ui/Settings/NetworkProxyPage.qml:14
#, kde-format
msgctxt "@title:window"
msgid "Network Proxy"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:27
#: content/ui/Settings/SettingsPage.qml:45
#, kde-format
msgid "Network Proxy"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:32
#, kde-format
msgid "System Default"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:44
#, kde-format
msgid "HTTP"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:56
#, kde-format
msgid "Socks5"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:76
#, kde-format
msgid "Host"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:89
#, kde-format
msgid "Port"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:110
#, fuzzy, kde-format
#| msgid "Username:"
msgid "User"
msgstr "Användarnamn:"

#: content/ui/Settings/NetworkProxyPage.qml:120
#, kde-format
msgid "Password"
msgstr ""

#: content/ui/Settings/NetworkProxyPage.qml:140
#: content/ui/Settings/ProfileEditor.qml:356
#, kde-format
msgid "Apply"
msgstr "Verkställ"

#: content/ui/Settings/PreferencesCard.qml:22
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgctxt "@label Settings header"
msgid "Preferences"
msgstr "Valt standardspråk:"

#: content/ui/Settings/PreferencesCard.qml:30
#, kde-format
msgctxt "@label Account preferences"
msgid ""
"These preferences apply to the current account and are synced to other "
"clients."
msgstr ""

#: content/ui/Settings/PreferencesCard.qml:36
#, kde-format
msgctxt "@label Account preferences"
msgid "Mark uploaded media as sensitive by default"
msgstr ""

#: content/ui/Settings/PreferencesCard.qml:48
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgctxt "@label Account preferences"
msgid "Default post language"
msgstr "Valt standardspråk:"

#: content/ui/Settings/PreferencesCard.qml:64
#, kde-format
msgctxt "@label Account preferences"
msgid "Default post visibility"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:23
#, kde-format
msgid "Profile Editor"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:31
#: content/ui/StatusComposer/StatusComposer.qml:208
#, kde-format
msgid "Please choose a file"
msgstr "Välj en fil"

#: content/ui/Settings/ProfileEditor.qml:116
#, kde-format
msgid "Display Name"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:129
#, kde-format
msgid "Bio"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:148
#, kde-format
msgid "Header"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:182
#, kde-format
msgid "PNG, GIF or JPG. At most 2 MB. Will be downscaled to 1500x500px"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:195
#: content/ui/Settings/ProfileEditor.qml:257
#: content/ui/StatusDelegate/OverflowMenu.qml:65
#, fuzzy, kde-format
#| msgid "Delete word"
msgid "Delete"
msgstr "Ta bort ord"

#: content/ui/Settings/ProfileEditor.qml:210
#, kde-format
msgid "Avatar"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:275
#, kde-format
msgid "Default status privacy"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:278
#, fuzzy, kde-format
#| msgid "Public"
msgid "Public post"
msgstr "Öppen"

#: content/ui/Settings/ProfileEditor.qml:282
#, fuzzy, kde-format
#| msgid "Unlisted"
msgid "Unlisted post"
msgstr "Olistad"

#: content/ui/Settings/ProfileEditor.qml:286
#, fuzzy, kde-format
#| msgid "Follow"
msgid "Followers-only post"
msgstr "Följ"

#: content/ui/Settings/ProfileEditor.qml:290
#, fuzzy, kde-format
#| msgid "Direct Message"
msgid "Direct post"
msgstr "Direkt meddelande"

#: content/ui/Settings/ProfileEditor.qml:309
#, kde-format
msgid "Mark by default content as sensitive"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:317
#, kde-format
msgid "Require follow requests"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:325
#, kde-format
msgid "This is a bot account"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:333
#, kde-format
msgid "Suggest account to others"
msgstr ""

#: content/ui/Settings/ProfileEditor.qml:349
#, kde-format
msgid "Reset"
msgstr ""

#: content/ui/Settings/SettingsPage.qml:13
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgctxt "@title:window"
msgid "Settings"
msgstr "Inställningar"

#: content/ui/Settings/SettingsPage.qml:68
#, fuzzy, kde-format
#| msgid "Tokodon"
msgid "About Tokodon"
msgstr "Tokodon"

#: content/ui/Settings/SettingsPage.qml:75
#, kde-format
msgid "About KDE"
msgstr ""

#: content/ui/Settings/SonnetCard.qml:24
#, fuzzy, kde-format
#| msgid "Spell Checking"
msgid "Spellchecking"
msgstr "Stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:30
#, kde-format
msgid "Enable automatic spell checking"
msgstr "Aktivera automatisk stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:42
#, kde-format
msgid "Ignore uppercase words"
msgstr "Ignorera ord med stora bokstäver"

#: content/ui/Settings/SonnetCard.qml:54
#, kde-format
msgid "Ignore hyphenated words"
msgstr "Ignorera ord med bindestreck"

#: content/ui/Settings/SonnetCard.qml:66
#, kde-format
msgid "Detect language automatically"
msgstr "Identifiera automatiskt språk"

#: content/ui/Settings/SonnetCard.qml:77
#, kde-format
msgid "Selected default language:"
msgstr "Valt standardspråk:"

#: content/ui/Settings/SonnetCard.qml:78
#, kde-format
msgid "None"
msgstr ""

#: content/ui/Settings/SonnetCard.qml:97
#, fuzzy, kde-format
#| msgid "Spell checking languages"
msgid "Additional Spell Checking Languages"
msgstr "Språk för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:98
#, kde-format
msgid ""
"%1 will provide spell checking and suggestions for the languages listed here "
"when autodetection is enabled."
msgstr ""
"%1 tillhandahåller stavningskontroll och förslag för språken som listas här "
"när automatisk identifiering är aktiverad."

#: content/ui/Settings/SonnetCard.qml:109
#, kde-format
msgid "Open Personal Dictionary"
msgstr "Öppna personlig ordlista"

#: content/ui/Settings/SonnetCard.qml:120
#, fuzzy, kde-format
#| msgid "Spell checking languages"
msgctxt "@title:window"
msgid "Spell checking languages"
msgstr "Språk för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:130
#: content/ui/Settings/SonnetCard.qml:140
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgid "Default Language"
msgstr "Valt standardspråk:"

#: content/ui/Settings/SonnetCard.qml:152
#, kde-format
msgid "Spell checking dictionary"
msgstr "Ordlista för stavningskontroll"

#: content/ui/Settings/SonnetCard.qml:159
#, kde-format
msgid "Add a new word to your personal dictionary…"
msgstr "Lägg till ett nytt ord i din personliga ordlista…"

#: content/ui/Settings/SonnetCard.qml:162
#, fuzzy, kde-format
#| msgctxt "@action:button"
#| msgid "Add word"
msgctxt "@action:button"
msgid "Add Word"
msgstr "Lägg till ord"

#: content/ui/Settings/SonnetCard.qml:189
#, kde-format
msgid "Delete word"
msgstr "Ta bort ord"

#: content/ui/SocialGraphPage.qml:97
#, fuzzy, kde-format
#| msgid "Follow"
msgid "No follow requests"
msgstr "Följ"

#: content/ui/SocialGraphPage.qml:99
#, fuzzy, kde-format
#| msgid "%1 followers"
msgid "No followers"
msgstr "%1 följare"

#: content/ui/SocialGraphPage.qml:101
#, fuzzy, kde-format
#| msgid "%1 following"
msgid "Not following anyone"
msgstr "%1 följer"

#: content/ui/StatusComposer/AttachmentGrid.qml:65
#: content/ui/StatusDelegate/OverflowMenu.qml:58
#, kde-format
msgid "Edit"
msgstr ""

#: content/ui/StatusComposer/AttachmentGrid.qml:99
#, kde-format
msgid "Remove"
msgstr ""

#: content/ui/StatusComposer/AttachmentInfoDialog.qml:32
#, kde-format
msgid "Add a description"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:35
#, fuzzy, kde-format
#| msgid "Choice %1"
msgctxt "@label:textbox Poll choice"
msgid "Choice %1"
msgstr "Val %1"

#: content/ui/StatusComposer/Poll.qml:59
#, fuzzy, kde-format
#| msgid "Choice %1"
msgctxt "@action:intoolbar Poll toolbar"
msgid "Add Choice"
msgstr "Val %1"

#: content/ui/StatusComposer/Poll.qml:60
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Add a new poll choice"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:79
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "When the poll will expire"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:88
#, kde-format
msgctxt "@option:check Poll toolbar"
msgid "Multiple choice"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:94
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Allow multiple choices"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:103
#, kde-format
msgctxt "@option:check Poll toolbar"
msgid "Hide totals"
msgstr ""

#: content/ui/StatusComposer/Poll.qml:109
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Hide vote count until the poll ends"
msgstr ""

#: content/ui/StatusComposer/StatusComposer.qml:45
#, fuzzy, kde-format
#| msgid "Write a new toot"
msgid "Edit this post"
msgstr "Skriv en ny tuta"

#: content/ui/StatusComposer/StatusComposer.qml:47
#, fuzzy, kde-format
#| msgid "Write a new toot"
msgid "Reply to this post"
msgstr "Skriv en ny tuta"

#: content/ui/StatusComposer/StatusComposer.qml:49
#, fuzzy, kde-format
#| msgid "Write a new toot"
msgid "Rewrite this post"
msgstr "Skriv en ny tuta"

#: content/ui/StatusComposer/StatusComposer.qml:51
#, fuzzy, kde-format
#| msgid "Write a new toot"
msgid "Write a new post"
msgstr "Skriv en ny tuta"

#: content/ui/StatusComposer/StatusComposer.qml:93
#: content/ui/StatusComposer/StatusComposer.qml:277
#, kde-format
msgid "Content Warning"
msgstr "Innehållsvarning"

#: content/ui/StatusComposer/StatusComposer.qml:109
#, kde-format
msgid "What's new?"
msgstr "Vad är en nytt?"

#: content/ui/StatusComposer/StatusComposer.qml:211
#, kde-format
msgid "Attach File"
msgstr "Bifoga fil"

#: content/ui/StatusComposer/StatusComposer.qml:221
#, kde-format
msgid "Add Poll"
msgstr "Bifoga enkät"

#: content/ui/StatusComposer/StatusComposer.qml:249
#: content/ui/StatusDelegate/StatusDelegate.qml:447
#: content/ui/StatusDelegate/StatusDelegate.qml:455
#, kde-format
msgid "Public"
msgstr "Öppen"

#: content/ui/StatusComposer/StatusComposer.qml:254
#: content/ui/StatusDelegate/StatusDelegate.qml:449
#, kde-format
msgid "Unlisted"
msgstr "Olistad"

#: content/ui/StatusComposer/StatusComposer.qml:259
#: content/ui/StatusDelegate/StatusDelegate.qml:451
#, kde-format
msgid "Private"
msgstr "Privat"

#: content/ui/StatusComposer/StatusComposer.qml:264
#: content/ui/StatusDelegate/StatusDelegate.qml:453
#, kde-format
msgid "Direct Message"
msgstr "Direkt meddelande"

#: content/ui/StatusComposer/StatusComposer.qml:269
#: content/ui/StatusDelegate/StatusDelegate.qml:443
#, kde-format
msgid "Visibility"
msgstr "Synlighet"

#: content/ui/StatusComposer/StatusComposer.qml:274
#, kde-format
msgctxt "Short for content warning"
msgid "cw"
msgstr "iv"

#: content/ui/StatusComposer/StatusComposer.qml:284
#, kde-format
msgctxt "@info:whatsthis Post language selection"
msgid "Select the language the post is written in"
msgstr ""

#: content/ui/StatusComposer/StatusComposer.qml:290
#, fuzzy, kde-format
#| msgid "Selected default language:"
msgctxt "@label:listbox Post language selection"
msgid "Post Language"
msgstr "Valt standardspråk:"

#: content/ui/StatusComposer/StatusComposer.qml:305
#, kde-format
msgctxt "@label Character count in the status composer"
msgid "<b>%1/%2</b> characters"
msgstr ""

#: content/ui/StatusComposer/StatusComposer.qml:313
#, kde-format
msgid "Send"
msgstr "Skicka"

#: content/ui/StatusDelegate/AttachmentGrid.qml:118
#, kde-format
msgid "Not available"
msgstr ""

#: content/ui/StatusDelegate/AttachmentGrid.qml:213
#, kde-format
msgid "Media Hidden"
msgstr ""

#: content/ui/StatusDelegate/InlineIdentityInfo.qml:25
#: content/ui/StatusDelegate/StatusDelegate.qml:150
#: content/ui/StatusDelegate/StatusDelegate.qml:250
#, fuzzy, kde-format
#| msgid "Feature on profile"
msgid "View profile"
msgstr "Presentera för profil"

#: content/ui/StatusDelegate/OverflowMenu.qml:21
#, kde-format
msgid "Expand This Post"
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:28
#, kde-format
msgid "Open Original Page"
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:36
#, kde-format
msgid "Copy Link to This Post"
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:39
#, kde-format
msgid "Post link copied."
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:47
#: content/ui/StatusDelegate/StatusDelegate.qml:399
#, kde-format
msgid "Remove bookmark"
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:47
#, kde-format
msgid "Bookmark"
msgstr ""

#: content/ui/StatusDelegate/OverflowMenu.qml:72
#, kde-format
msgid "Delete & Re-draft"
msgstr ""

#: content/ui/StatusDelegate/Poll.qml:39
#, fuzzy, kde-format
#| msgctxt "hour:minute"
#| msgid "%1:%2"
msgctxt "Votes percentage"
msgid "%1%"
msgstr "%1:%2"

#: content/ui/StatusDelegate/Poll.qml:63
#, kde-format
msgid "(No votes)"
msgstr ""

#: content/ui/StatusDelegate/Poll.qml:97
#, kde-format
msgid "Vote"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:113
#, kde-format
msgid "Filtered: %1"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:117
#, kde-format
msgid "Show anyway"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:164
#, kde-format
msgid "Your poll has ended"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:166
#, kde-format
msgid "A poll you voted in has ended"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:191
#, kde-format
msgid "Pinned entry"
msgstr "Fäst post"

#: content/ui/StatusDelegate/StatusDelegate.qml:258
#, kde-format
msgid "%1 boosted"
msgstr "%1 förstärkte"

#: content/ui/StatusDelegate/StatusDelegate.qml:260
#, kde-format
msgid "In reply to %1"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:260
#, fuzzy, kde-format
#| msgid "%1 favorited your post"
msgid "%1 replied to your post"
msgstr "%1 markerade ditt inlägg som favorit"

#: content/ui/StatusDelegate/StatusDelegate.qml:303
#, fuzzy, kde-format
#| msgid "Show more"
msgid "Show Less"
msgstr "Visa fler"

#: content/ui/StatusDelegate/StatusDelegate.qml:303
#, fuzzy, kde-format
#| msgid "Show more"
msgid "Show More"
msgstr "Visa fler"

#: content/ui/StatusDelegate/StatusDelegate.qml:357
#, kde-format
msgctxt "Reply to a post"
msgid "Reply"
msgstr "Svara"

#: content/ui/StatusDelegate/StatusDelegate.qml:358
#, kde-format
msgctxt "More than one reply"
msgid "1+"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:372
#, kde-format
msgctxt "Share a post"
msgid "Boost"
msgstr "Förstärk"

#: content/ui/StatusDelegate/StatusDelegate.qml:386
#, kde-format
msgctxt "Like a post"
msgid "Like"
msgstr "Gilla"

#: content/ui/StatusDelegate/StatusDelegate.qml:399
#, kde-format
msgctxt "Bookmark a post"
msgid "Bookmark"
msgstr ""

#: content/ui/StatusDelegate/StatusDelegate.qml:411
#, fuzzy, kde-format
#| msgid "Show more"
msgctxt "Show more options"
msgid "More"
msgstr "Visa fler"

#: content/ui/StatusDelegate/StatusDelegate.qml:472
#, kde-format
msgid "via %1"
msgstr ""

#: content/ui/TimelinePage.qml:110
#, kde-format
msgid "No posts"
msgstr ""

#: content/ui/UserInfo.qml:72
#, kde-format
msgid "Log in to an existing account"
msgstr ""

#: content/ui/UserInfo.qml:195
#, kde-format
msgid "Switch user"
msgstr ""

#: content/ui/UserInfo.qml:207
#, kde-format
msgid "Add"
msgstr ""

#: content/ui/UserInfo.qml:219
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Settings"
msgid "Open settings"
msgstr "Inställningar"

#: conversation/conversationmodel.cpp:59
#, kde-format
msgid "Empty conversation"
msgstr ""

#: conversation/conversationmodel.cpp:63
#, kde-format
msgid "%1 and %2"
msgstr ""

#: conversation/conversationmodel.cpp:65
#, kde-format
msgid "%2 and one other"
msgid_plural "%2 and %1 others"
msgstr[0] ""
msgstr[1] ""

#: editor/polltimemodel.cpp:12
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "5 minutes"
msgstr ""

#: editor/polltimemodel.cpp:13
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "30 minutes"
msgstr ""

#: editor/polltimemodel.cpp:14
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "1 hour"
msgstr ""

#: editor/polltimemodel.cpp:15
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "6 hours"
msgstr ""

#: editor/polltimemodel.cpp:16
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "12 hours"
msgstr ""

#: editor/polltimemodel.cpp:17
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "1 day"
msgstr ""

#: editor/polltimemodel.cpp:18
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "3 days"
msgstr ""

#: editor/polltimemodel.cpp:19
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "7 days"
msgstr ""

#: editor/posteditorbackend.cpp:240
#, kde-format
msgid "An unknown error occurred."
msgstr ""

#: main.cpp:112
#, kde-format
msgid "Tokodon"
msgstr "Tokodon"

#: main.cpp:114
#, kde-format
msgid "Mastodon client"
msgstr "Mastodon-klient"

#: main.cpp:116
#, fuzzy, kde-format
#| msgid "© 2021 Carl Schwan, 2021 KDE Community"
msgid "© 2021-2023 Carl Schwan, 2021-2023 KDE Community"
msgstr "© 2021 Carl Schwan, 2021 KDE-gemenskapen"

#: main.cpp:117
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:117
#, kde-format
msgid "Maintainer"
msgstr ""

#: main.cpp:118
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luna Jernberg,Stefan Asserhäll"

#: main.cpp:118
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "droidbittin@gmail.com,stefan.asserhall@bredband.net"

#: main.cpp:126
#, kde-format
msgid "Client for the decentralized social network: mastodon"
msgstr "Klient för det decentraliserade sociala nätverket: mastodon"

#: main.cpp:127
#, kde-format
msgid "Supports web+ap: url scheme"
msgstr ""

#: search/searchmodel.cpp:137
#, kde-format
msgid "People"
msgstr ""

#: search/searchmodel.cpp:139
#, kde-format
msgid "Hashtags"
msgstr ""

#: timeline/accountmodel.cpp:34 timeline/maintimelinemodel.cpp:27
#, kde-format
msgid "Loading"
msgstr "Läser in"

#: timeline/linkpaginatedtimelinemodel.cpp:25 timeline/maintimelinemodel.cpp:38
#, kde-format
msgctxt "@title"
msgid "Bookmarks"
msgstr ""

#: timeline/linkpaginatedtimelinemodel.cpp:27 timeline/maintimelinemodel.cpp:40
#, kde-format
msgctxt "@title"
msgid "Favourites"
msgstr ""

#: timeline/linkpaginatedtimelinemodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Trending"
msgstr ""

#: timeline/maintimelinemodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Home (%1)"
msgstr "Hem (%1)"

#: timeline/maintimelinemodel.cpp:31
#, kde-format
msgctxt "@title"
msgid "Home"
msgstr "Hem"

#: timeline/maintimelinemodel.cpp:34
#, kde-format
msgctxt "@title"
msgid "Local Timeline"
msgstr "Lokal tidslinje"

#: timeline/maintimelinemodel.cpp:36
#, kde-format
msgctxt "@title"
msgid "Global Timeline"
msgstr "Global tidslinje"

#: timeline/notificationmodel.cpp:99
#, kde-format
msgid "Error occurred when fetching the latest notification."
msgstr ""

#: timeline/post.cpp:309
#, kde-format
msgid "in the future"
msgstr ""

#: timeline/post.cpp:311
#, fuzzy, kde-format
#| msgid "%1h"
msgid "%1s"
msgstr "%1 t"

#: timeline/post.cpp:313
#, fuzzy, kde-format
#| msgid "%1h"
msgid "%1m"
msgstr "%1 t"

#: timeline/post.cpp:315
#, kde-format
msgid "%1h"
msgstr "%1 t"

#: timeline/post.cpp:317
#, kde-format
msgid "%1d"
msgstr "%1 d"

#: timeline/post.cpp:321
#, kde-format
msgid "1 week ago"
msgstr ""

#: timeline/post.cpp:323
#, kde-format
msgid "%1 weeks ago"
msgstr ""

#: timeline/post.cpp:327
#, kde-format
msgid "1 month ago"
msgstr ""

#: timeline/post.cpp:329
#, kde-format
msgid "%1 months ago"
msgstr ""

#: timeline/post.cpp:335
#, kde-format
msgid "1 year ago"
msgstr ""

#: timeline/post.cpp:337
#, kde-format
msgid "%1 years ago"
msgstr ""

#: timeline/threadmodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Thread"
msgstr "Tråd"

#: utils/filetransferjob.cpp:82
#, fuzzy, kde-format
#| msgid "Loading"
msgctxt "Job heading, like 'Copying'"
msgid "Downloading"
msgstr "Läser in"

#: utils/filetransferjob.cpp:83
#, kde-format
msgctxt "The URL being downloaded/uploaded"
msgid "Source"
msgstr ""

#: utils/filetransferjob.cpp:84
#, fuzzy, kde-format
#| msgctxt "Show only mentions"
#| msgid "Mentions"
msgctxt "The location being downloaded to"
msgid "Destination"
msgstr "Omnämnanden"

#~ msgid "Toot"
#~ msgstr "Tut"

#~ msgid "Close"
#~ msgstr "Stäng"

#~ msgid "Previous image"
#~ msgstr "Föregående bild"

#~ msgid "Next image"
#~ msgstr "Nästa bild"

#~ msgid "%1 toots"
#~ msgstr "%1 tutor"

#~ msgctxt "hour:minute"
#~ msgid "%1:%2"
#~ msgstr "%1:%2"

#~ msgid "Make pool auto-exclusive"
#~ msgstr "Gör omröstning automatiskt exklusiv"

#~ msgid "Username:"
#~ msgstr "Användarnamn:"

#~ msgid "Image View"
#~ msgstr "Bildvy"

#~ msgid "Local Timeline"
#~ msgstr "Lokal tidslinje"

#~ msgid "Global Timeline"
#~ msgstr "Global tidslinje"

#~ msgid "Add an account"
#~ msgstr "Lägg till ett konto"

#~ msgid "Options:"
#~ msgstr "Alternativ:"

#~ msgid "Notifications support is not implemented yet"
#~ msgstr "Stöd för underrättelser är inte ännu implementerat"

#~ msgid "Shared by %1"
#~ msgstr "Delas av %1"
