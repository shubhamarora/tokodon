# Translation of tokodon.po to Catalan (Valencian)
# Copyright (C) 2021-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022, 2023.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: tokodon\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-06 00:47+0000\n"
"PO-Revision-Date: 2023-05-27 16:04+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 23.07.70\n"

#: account/abstractaccount.cpp:430
#, kde-format
msgid "Could not follow account"
msgstr "No s'ha pogut seguir el compte"

#: account/abstractaccount.cpp:431
#, kde-format
msgid "Could not unfollow account"
msgstr "No s'ha pogut desfer el seguiment del compte"

#: account/abstractaccount.cpp:432
#, kde-format
msgid "Could not block account"
msgstr "No s'ha pogut bloquejar el compte"

#: account/abstractaccount.cpp:433
#, kde-format
msgid "Could not unblock account"
msgstr "No s'ha pogut desbloquejar el compte"

#: account/abstractaccount.cpp:434
#, kde-format
msgid "Could not mute account"
msgstr "No s'ha pogut silenciar el compte"

#: account/abstractaccount.cpp:435
#, kde-format
msgid "Could not unmute account"
msgstr "No s'ha pogut desfer el silenciament del compte"

#: account/abstractaccount.cpp:436
#, kde-format
msgid "Could not feature account"
msgstr "No s'ha pogut destacar el compte"

#: account/abstractaccount.cpp:437
#, kde-format
msgid "Could not unfeature account"
msgstr "No s'ha pogut desfer el fet de destacar el compte"

#: account/abstractaccount.cpp:438
#, kde-format
msgid "Could not edit note about an account"
msgstr "No s'ha pogut editar una nota quant al compte"

#: account/notificationhandler.cpp:23
#: content/ui/StatusDelegate/StatusDelegate.qml:159
#, kde-format
msgid "%1 favorited your post"
msgstr "%1 ha marcat la vostra publicació com a preferida"

#: account/notificationhandler.cpp:27 content/ui/FollowDelegate.qml:44
#: content/ui/FollowDelegate.qml:59
#, kde-format
msgid "%1 followed you"
msgstr "%1 vos seguix"

#: account/notificationhandler.cpp:31
#: content/ui/StatusDelegate/StatusDelegate.qml:258
#, kde-format
msgid "%1 boosted your post"
msgstr "%1 ha impulsat la vostra publicació"

#: account/notificationhandler.cpp:39
#: content/ui/StatusDelegate/StatusDelegate.qml:161
#, kde-format
msgid "%1 edited a post"
msgstr "%1 ha editat una publicació"

#: account/profileeditor.cpp:183
#, kde-format
msgid "Image is too big"
msgstr "La imatge és massa gran"

#: account/profileeditor.cpp:196
#, kde-format
msgid "Unsupported image file. Only jpeg, png and gif are supported."
msgstr "Fitxer d'imatge no permés. Només es permeten JPEG, PNG i GIF."

#: account/profileeditor.cpp:323
#, kde-format
msgid "Account details saved"
msgstr "S'han guardat els detalls del compte"

#: account/socialgraphmodel.cpp:30
#, kde-format
msgctxt "@title"
msgid "Follow Requests"
msgstr "Sol·licituds de seguiment"

#: account/socialgraphmodel.cpp:32
#, kde-format
msgctxt "@title"
msgid "Followers"
msgstr "Seguidors"

#: account/socialgraphmodel.cpp:34
#, kde-format
msgctxt "@title"
msgid "Following"
msgstr "Seguint"

#: content/ui/AccountInfo.qml:70
#, kde-format
msgid "Follows you"
msgstr "Vos seguix"

#: content/ui/AccountInfo.qml:110
#, kde-format
msgid "Requested"
msgstr "Sol·licitat"

#: content/ui/AccountInfo.qml:113
#, kde-format
msgid "Following"
msgstr "Seguint"

#: content/ui/AccountInfo.qml:115
#, kde-format
msgid "Follow"
msgstr "Seguix"

#: content/ui/AccountInfo.qml:140
#, kde-format
msgid "Stop notifying me when %1 posts"
msgstr "Para notificar-me quan publique %1"

#: content/ui/AccountInfo.qml:142
#, kde-format
msgid "Notify me when %1 posts"
msgstr "Notifica'm quan publique %1"

#: content/ui/AccountInfo.qml:159
#, kde-format
msgid "Hide Boosts from %1"
msgstr "Oculta els impulsos de %1"

#: content/ui/AccountInfo.qml:161
#, kde-format
msgid "Stop Hiding Boosts from %1"
msgstr "Para l'ocultament dels impulsos de %1"

#: content/ui/AccountInfo.qml:178
#, kde-format
msgid "Stop Featuring on Profile"
msgstr "Para el fet de destacar en el perfil"

#: content/ui/AccountInfo.qml:180
#, kde-format
msgid "Feature on Profile"
msgstr "Destaca en el perfil"

#: content/ui/AccountInfo.qml:197
#, kde-format
msgid "Stop Muting"
msgstr "Para el silenci"

#: content/ui/AccountInfo.qml:199
#, kde-format
msgid "Mute"
msgstr "Silencia"

#: content/ui/AccountInfo.qml:216
#, kde-format
msgid "Stop Blocking"
msgstr "Para el bloqueig"

#: content/ui/AccountInfo.qml:218
#, kde-format
msgid "Block"
msgstr "Bloqueja"

#: content/ui/AccountInfo.qml:233
#, kde-format
msgid "Edit Profile"
msgstr "Edita un perfil"

#: content/ui/AccountInfo.qml:237 content/ui/Settings/AccountsCard.qml:36
#, kde-format
msgid "Account editor"
msgstr "Editor de comptes"

#: content/ui/AccountInfo.qml:244 content/ui/TimelinePage.qml:130
#, kde-format
msgid "Settings"
msgstr "Configuració"

#: content/ui/AccountInfo.qml:245 content/ui/TimelinePage.qml:132
#: content/ui/UserInfo.qml:218
#, kde-format
msgid "Configure"
msgstr "Configura"

#: content/ui/AccountInfo.qml:251 content/ui/main.qml:211
#, kde-format
msgid "Follow Requests"
msgstr "Sol·licituds de seguiment"

#: content/ui/AccountInfo.qml:328
#, kde-format
msgid "Note"
msgstr "Nota"

#: content/ui/AccountInfo.qml:336
#, kde-format
msgid "Saved"
msgstr "Guardat"

#: content/ui/AccountInfo.qml:351
#, kde-format
msgid "Click to add a note"
msgstr "Feu clic per a afegir una nota"

#: content/ui/AccountInfo.qml:427
#, kde-format
msgctxt "@label User's number of statuses"
msgid "<b>%1</b> posts"
msgstr "<b>%1</b> publicacions"

#: content/ui/AccountInfo.qml:439
#, kde-format
msgctxt "@label User's number of followers"
msgid "<b>%1</b> followers"
msgstr "<b>%1</b> seguidors"

#: content/ui/AccountInfo.qml:451
#, kde-format
msgctxt "@label User's number of followed accounts"
msgid "<b>%1</b> following"
msgstr "<b>%1</b> seguits"

#: content/ui/AccountInfo.qml:463
#, kde-format
msgctxt "@item:inmenu Profile Post Filter"
msgid "Posts"
msgstr "Publicacions"

#: content/ui/AccountInfo.qml:467
#, kde-format
msgctxt "@item:inmenu Profile Post Filter"
msgid "Posts && Replies"
msgstr "Publicacions i respostes"

#: content/ui/AccountInfo.qml:471
#, kde-format
msgctxt "@item:inmenu Profile Post Filter"
msgid "Media"
msgstr "Multimèdia"

#: content/ui/AccountInfo.qml:498
#, kde-format
msgctxt "@option:check"
msgid "Hide boosts"
msgstr "Oculta els impulsos"

#: content/ui/AuthorizationPage.qml:15
#, kde-format
msgid "Authorization"
msgstr "Autorització"

#: content/ui/AuthorizationPage.qml:29
#, kde-format
msgid "Authorize Tokodon to act on your behalf"
msgstr "Autoritza Tokodon a actuar en nom vostre"

#: content/ui/AuthorizationPage.qml:41
#, kde-format
msgid "To continue, please open the following link and authorize Tokodon: %1"
msgstr "Per a continuar, obriu l'enllaç següent i autoritzeu Tokodon: %1"

#: content/ui/AuthorizationPage.qml:57 content/ui/AuthorizationPage.qml:76
#, kde-format
msgid "Open Link"
msgstr "Obri l'enllaç"

#: content/ui/AuthorizationPage.qml:62 content/ui/AuthorizationPage.qml:84
#, kde-format
msgid "Copy Link"
msgstr "Copia l'enllaç"

#: content/ui/AuthorizationPage.qml:65 content/ui/AuthorizationPage.qml:87
#, kde-format
msgid "Link copied."
msgstr "S'ha copiat l'enllaç."

#: content/ui/AuthorizationPage.qml:101
#, kde-format
msgid "Enter token:"
msgstr "Introduïu el testimoni:"

#: content/ui/AuthorizationPage.qml:109 content/ui/LoginPage.qml:49
#, kde-format
msgid "Continue"
msgstr "Continua"

#: content/ui/AuthorizationPage.qml:112
#, kde-format
msgid "Please insert the generated token."
msgstr "Inseriu el testimoni generat."

#: content/ui/ConversationPage.qml:11
#, kde-format
msgid "Conversations"
msgstr "Converses"

#: content/ui/ConversationPage.qml:35
#, kde-format
msgid "No Conversations"
msgstr "Sense converses"

#: content/ui/ExplorePage.qml:15 content/ui/main.qml:304
#, kde-format
msgid "Explore"
msgstr "Explora"

#: content/ui/ExplorePage.qml:30 content/ui/NotificationPage.qml:30
#: content/ui/TimelinePage.qml:44 search/searchmodel.cpp:141
#, kde-format
msgid "Post"
msgstr "Publicació"

#: content/ui/ExplorePage.qml:84
#, kde-format
msgid "No Posts"
msgstr "Sense publicacions"

#: content/ui/LanguageSelector.qml:64
#, kde-format
msgctxt "@item:inlistbox Group of preferred languages"
msgid "Preferred Languages"
msgstr "Idiomes preferits"

#: content/ui/LanguageSelector.qml:65
#, kde-format
msgctxt "@item:inlistbox Group of all languages"
msgid "All Languages"
msgstr "Tots els idiomes"

#: content/ui/LoginPage.qml:14
#, kde-format
msgid "Login"
msgstr "Inici de sessió"

#: content/ui/LoginPage.qml:22 content/ui/TimelinePage.qml:66
#, kde-format
msgctxt "@info:status Network status"
msgid "Failed to contact server: %1. Please check your settings."
msgstr "No s'ha pogut contactar amb el servidor: %1. Reviseu la configuració."

#: content/ui/LoginPage.qml:35
#, kde-format
msgid "Welcome to Tokodon"
msgstr "Us donem la benvinguda a Tokodon"

#: content/ui/LoginPage.qml:40
#, kde-format
msgid "Instance Url:"
msgstr "URL de la instància:"

#: content/ui/LoginPage.qml:52
#, kde-format
msgid "Instance URL must not be empty!"
msgstr "L'URL de la instància no pot estar buit!"

#: content/ui/LoginPage.qml:76
#, kde-format
msgctxt "@title:group Login page"
msgid "Network Settings"
msgstr "Configureu la xarxa"

#: content/ui/LoginPage.qml:81
#, kde-format
msgctxt "@option:check Login page"
msgid "Ignore SSL errors"
msgstr "Ignora els errors d'SSL"

#: content/ui/LoginPage.qml:88 content/ui/Settings/NetworkProxyPage.qml:72
#, kde-format
msgid "Proxy Settings"
msgstr "Configureu el servidor intermediari"

#: content/ui/main.qml:182
#, kde-format
msgid "Home"
msgstr "Inici"

#: content/ui/main.qml:198 content/ui/NotificationPage.qml:15
#, kde-format
msgid "Notifications"
msgstr "Notificacions"

#: content/ui/main.qml:227
#, kde-format
msgid "Local"
msgstr "Local"

#: content/ui/main.qml:242
#, kde-format
msgid "Global"
msgstr "Global"

#: content/ui/main.qml:258
#, kde-format
msgid "Conversation"
msgstr "Conversa"

#: content/ui/main.qml:272
#, kde-format
msgid "Favourites"
msgstr "Preferides"

#: content/ui/main.qml:288
#, kde-format
msgid "Bookmarks"
msgstr "Punts"

#: content/ui/main.qml:318
#, kde-format
msgid "Search"
msgstr "Busca"

#: content/ui/NotificationPage.qml:37
#, kde-format
msgctxt "Show all notifications"
msgid "All"
msgstr "Totes"

#: content/ui/NotificationPage.qml:48
#, kde-format
msgctxt "Show only mentions"
msgid "Mentions"
msgstr "Mencions"

#: content/ui/NotificationPage.qml:57
#, kde-format
msgctxt "Show only favorites"
msgid "Favorites"
msgstr "Preferits"

#: content/ui/NotificationPage.qml:66
#, kde-format
msgctxt "Show only boosts"
msgid "Boosts"
msgstr "Impulsos"

#: content/ui/NotificationPage.qml:75
#, kde-format
msgctxt "Show only poll results"
msgid "Poll results"
msgstr "Resultats de votació"

#: content/ui/NotificationPage.qml:84
#, kde-format
msgctxt "Show only follows"
msgid "Follows"
msgstr "Seguix"

#: content/ui/NotificationPage.qml:196
#, kde-format
msgid "No Notifications"
msgstr "Sense notificacions"

#: content/ui/SearchPage.qml:16
#, kde-format
msgctxt "@title"
msgid "Search"
msgstr "Busca"

#: content/ui/SearchView.qml:33
#, kde-format
msgid "Loading..."
msgstr "S'està carregant..."

#: content/ui/SearchView.qml:41
#, kde-format
msgid "No search results"
msgstr "Busca sense cap resultat"

#: content/ui/SearchView.qml:49
#, kde-format
msgid "Search for peoples, tags and posts"
msgstr "Busca persones, etiquetes i publicacions"

#: content/ui/Settings/AccountsCard.qml:24
#, kde-format
msgid "Accounts"
msgstr "Comptes"

#: content/ui/Settings/AccountsCard.qml:72
#, kde-format
msgid "Logout"
msgstr "Eixida"

#: content/ui/Settings/AccountsCard.qml:92 content/ui/UserInfo.qml:69
#, kde-format
msgid "Add Account"
msgstr "Afig un compte"

#: content/ui/Settings/GeneralCard.qml:19
#, kde-format
msgid "General"
msgstr "General"

#: content/ui/Settings/GeneralCard.qml:24
#, kde-format
msgid "Show detailed statistics about posts"
msgstr "Mostra estadístiques detallades quant a les publicacions"

#: content/ui/Settings/GeneralCard.qml:37
#, kde-format
msgid "Show link preview"
msgstr "Mostra la vista prèvia de l'enllaç"

#: content/ui/Settings/GeneralCard.qml:50
#, kde-format
msgid "Crop images in the timeline to 16:9"
msgstr "Escapça les imatges en la línia de temps a 16:9"

#: content/ui/Settings/GeneralCard.qml:62
#, kde-format
msgid "Auto-play animated GIFs"
msgstr "Reproduïx automàticament els GIF animats"

#: content/ui/Settings/GeneralCard.qml:75
#, kde-format
msgid "Color theme"
msgstr "Tema de color"

#: content/ui/Settings/GeneralCard.qml:92
#, kde-format
msgid "Content font"
msgstr "Lletra del contingut"

#: content/ui/Settings/GeneralCard.qml:98
#, kde-format
msgid "Please choose a font"
msgstr "Seleccioneu una lletra"

#: content/ui/Settings/NetworkProxyPage.qml:14
#, kde-format
msgctxt "@title:window"
msgid "Network Proxy"
msgstr "Servidor intermediari de la xarxa"

#: content/ui/Settings/NetworkProxyPage.qml:27
#: content/ui/Settings/SettingsPage.qml:45
#, kde-format
msgid "Network Proxy"
msgstr "Servidor intermediari de la xarxa"

#: content/ui/Settings/NetworkProxyPage.qml:32
#, kde-format
msgid "System Default"
msgstr "Predeterminada del sistema"

#: content/ui/Settings/NetworkProxyPage.qml:44
#, kde-format
msgid "HTTP"
msgstr "HTTP"

#: content/ui/Settings/NetworkProxyPage.qml:56
#, kde-format
msgid "Socks5"
msgstr "Socks5"

#: content/ui/Settings/NetworkProxyPage.qml:76
#, kde-format
msgid "Host"
msgstr "Servidor"

#: content/ui/Settings/NetworkProxyPage.qml:89
#, kde-format
msgid "Port"
msgstr "Port"

#: content/ui/Settings/NetworkProxyPage.qml:110
#, kde-format
msgid "User"
msgstr "Usuari"

#: content/ui/Settings/NetworkProxyPage.qml:120
#, kde-format
msgid "Password"
msgstr "Contrasenya"

#: content/ui/Settings/NetworkProxyPage.qml:140
#: content/ui/Settings/ProfileEditor.qml:356
#, kde-format
msgid "Apply"
msgstr "Aplica"

#: content/ui/Settings/PreferencesCard.qml:22
#, kde-format
msgctxt "@label Settings header"
msgid "Preferences"
msgstr "Preferències"

#: content/ui/Settings/PreferencesCard.qml:30
#, kde-format
msgctxt "@label Account preferences"
msgid ""
"These preferences apply to the current account and are synced to other "
"clients."
msgstr ""
"Estes preferències s'apliquen al compte actual i se sincronitzen amb els "
"altres clients."

#: content/ui/Settings/PreferencesCard.qml:36
#, kde-format
msgctxt "@label Account preferences"
msgid "Mark uploaded media as sensitive by default"
msgstr ""
"Marca els fitxers multimèdia pujats com a confidencials de manera "
"predeterminada"

#: content/ui/Settings/PreferencesCard.qml:48
#, kde-format
msgctxt "@label Account preferences"
msgid "Default post language"
msgstr "Idioma predeterminat de les publicacions"

#: content/ui/Settings/PreferencesCard.qml:64
#, kde-format
msgctxt "@label Account preferences"
msgid "Default post visibility"
msgstr "Visibilitat predeterminada de les publicacions"

#: content/ui/Settings/ProfileEditor.qml:23
#, kde-format
msgid "Profile Editor"
msgstr "Editor de perfils"

#: content/ui/Settings/ProfileEditor.qml:31
#: content/ui/StatusComposer/StatusComposer.qml:208
#, kde-format
msgid "Please choose a file"
msgstr "Seleccioneu un fitxer"

#: content/ui/Settings/ProfileEditor.qml:116
#, kde-format
msgid "Display Name"
msgstr "Nom que s'ha de mostrar"

#: content/ui/Settings/ProfileEditor.qml:129
#, kde-format
msgid "Bio"
msgstr "Biografia"

#: content/ui/Settings/ProfileEditor.qml:148
#, kde-format
msgid "Header"
msgstr "Capçalera"

#: content/ui/Settings/ProfileEditor.qml:182
#, kde-format
msgid "PNG, GIF or JPG. At most 2 MB. Will be downscaled to 1500x500px"
msgstr "PNG, GIF o JPG. Màxim 2 MB. L'escala es reduirà a 1500x500px"

#: content/ui/Settings/ProfileEditor.qml:195
#: content/ui/Settings/ProfileEditor.qml:257
#: content/ui/StatusDelegate/OverflowMenu.qml:65
#, kde-format
msgid "Delete"
msgstr "Suprimix"

#: content/ui/Settings/ProfileEditor.qml:210
#, kde-format
msgid "Avatar"
msgstr "Avatar"

#: content/ui/Settings/ProfileEditor.qml:275
#, kde-format
msgid "Default status privacy"
msgstr "Estat predeterminat de privadesa"

#: content/ui/Settings/ProfileEditor.qml:278
#, kde-format
msgid "Public post"
msgstr "Publicació pública"

#: content/ui/Settings/ProfileEditor.qml:282
#, kde-format
msgid "Unlisted post"
msgstr "Publicació no llistada"

#: content/ui/Settings/ProfileEditor.qml:286
#, kde-format
msgid "Followers-only post"
msgstr "Publicació només per als seguidors"

#: content/ui/Settings/ProfileEditor.qml:290
#, kde-format
msgid "Direct post"
msgstr "Publicació directa"

#: content/ui/Settings/ProfileEditor.qml:309
#, kde-format
msgid "Mark by default content as sensitive"
msgstr "Marca el contingut com a delicat de manera predeterminada"

#: content/ui/Settings/ProfileEditor.qml:317
#, kde-format
msgid "Require follow requests"
msgstr "Requerix sol·licituds de seguiment"

#: content/ui/Settings/ProfileEditor.qml:325
#, kde-format
msgid "This is a bot account"
msgstr "Este és un compte de bot"

#: content/ui/Settings/ProfileEditor.qml:333
#, kde-format
msgid "Suggest account to others"
msgstr "Suggerix el compte a altres"

#: content/ui/Settings/ProfileEditor.qml:349
#, kde-format
msgid "Reset"
msgstr "Reinicia"

#: content/ui/Settings/SettingsPage.qml:13
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "Configuració"

#: content/ui/Settings/SettingsPage.qml:68
#, kde-format
msgid "About Tokodon"
msgstr "Quant a Tokodon"

#: content/ui/Settings/SettingsPage.qml:75
#, kde-format
msgid "About KDE"
msgstr "Quant a KDE"

#: content/ui/Settings/SonnetCard.qml:24
#, kde-format
msgid "Spellchecking"
msgstr "Verificació de l'ortografia"

#: content/ui/Settings/SonnetCard.qml:30
#, kde-format
msgid "Enable automatic spell checking"
msgstr "Activa la verificació ortogràfica automàtica"

#: content/ui/Settings/SonnetCard.qml:42
#, kde-format
msgid "Ignore uppercase words"
msgstr "Ignora les paraules en majúscules"

#: content/ui/Settings/SonnetCard.qml:54
#, kde-format
msgid "Ignore hyphenated words"
msgstr "Ignora les paraules amb guió"

#: content/ui/Settings/SonnetCard.qml:66
#, kde-format
msgid "Detect language automatically"
msgstr "Detecta automàticament l'idioma"

#: content/ui/Settings/SonnetCard.qml:77
#, kde-format
msgid "Selected default language:"
msgstr "Idioma predeterminat seleccionat:"

#: content/ui/Settings/SonnetCard.qml:78
#, kde-format
msgid "None"
msgstr "Sense"

#: content/ui/Settings/SonnetCard.qml:97
#, kde-format
msgid "Additional Spell Checking Languages"
msgstr "Idiomes addicionals de verificació ortogràfica"

#: content/ui/Settings/SonnetCard.qml:98
#, kde-format
msgid ""
"%1 will provide spell checking and suggestions for the languages listed here "
"when autodetection is enabled."
msgstr ""
"%1 proporcionarà la verificació ortogràfica i suggeriments per als idiomes "
"llistats ací si s'ha activat la detecció automàtica."

#: content/ui/Settings/SonnetCard.qml:109
#, kde-format
msgid "Open Personal Dictionary"
msgstr "Obri un diccionari personal"

#: content/ui/Settings/SonnetCard.qml:120
#, kde-format
msgctxt "@title:window"
msgid "Spell checking languages"
msgstr "Idiomes de verificació ortogràfica"

#: content/ui/Settings/SonnetCard.qml:130
#: content/ui/Settings/SonnetCard.qml:140
#, kde-format
msgid "Default Language"
msgstr "Idioma predeterminat"

#: content/ui/Settings/SonnetCard.qml:152
#, kde-format
msgid "Spell checking dictionary"
msgstr "Diccionari de verificació ortogràfica"

#: content/ui/Settings/SonnetCard.qml:159
#, kde-format
msgid "Add a new word to your personal dictionary…"
msgstr "Afig una paraula nova al vostre diccionari personal…"

#: content/ui/Settings/SonnetCard.qml:162
#, kde-format
msgctxt "@action:button"
msgid "Add Word"
msgstr "Afig una paraula"

#: content/ui/Settings/SonnetCard.qml:189
#, kde-format
msgid "Delete word"
msgstr "Suprimix una paraula"

#: content/ui/SocialGraphPage.qml:97
#, kde-format
msgid "No follow requests"
msgstr "Sense sol·licituds de seguiment"

#: content/ui/SocialGraphPage.qml:99
#, kde-format
msgid "No followers"
msgstr "Sense seguidors"

#: content/ui/SocialGraphPage.qml:101
#, kde-format
msgid "Not following anyone"
msgstr "No seguiu a ningú"

#: content/ui/StatusComposer/AttachmentGrid.qml:65
#: content/ui/StatusDelegate/OverflowMenu.qml:58
#, kde-format
msgid "Edit"
msgstr "Edita"

#: content/ui/StatusComposer/AttachmentGrid.qml:99
#, kde-format
msgid "Remove"
msgstr "Elimina"

#: content/ui/StatusComposer/AttachmentInfoDialog.qml:32
#, kde-format
msgid "Add a description"
msgstr "Afig una descripció"

#: content/ui/StatusComposer/Poll.qml:35
#, kde-format
msgctxt "@label:textbox Poll choice"
msgid "Choice %1"
msgstr "Opció %1"

#: content/ui/StatusComposer/Poll.qml:59
#, kde-format
msgctxt "@action:intoolbar Poll toolbar"
msgid "Add Choice"
msgstr "Afig una opció"

#: content/ui/StatusComposer/Poll.qml:60
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Add a new poll choice"
msgstr "Afig una opció nova de votació"

#: content/ui/StatusComposer/Poll.qml:79
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "When the poll will expire"
msgstr "Quan caducarà la votació"

#: content/ui/StatusComposer/Poll.qml:88
#, kde-format
msgctxt "@option:check Poll toolbar"
msgid "Multiple choice"
msgstr "Elecció múltiple"

#: content/ui/StatusComposer/Poll.qml:94
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Allow multiple choices"
msgstr "Permet opcions múltiples"

#: content/ui/StatusComposer/Poll.qml:103
#, kde-format
msgctxt "@option:check Poll toolbar"
msgid "Hide totals"
msgstr "Oculta els totals"

#: content/ui/StatusComposer/Poll.qml:109
#, kde-format
msgctxt "@info:tooltip Poll toolbar"
msgid "Hide vote count until the poll ends"
msgstr "Oculta el comptador de vots fins que acabe la votació"

#: content/ui/StatusComposer/StatusComposer.qml:45
#, kde-format
msgid "Edit this post"
msgstr "Edita esta publicació"

#: content/ui/StatusComposer/StatusComposer.qml:47
#, kde-format
msgid "Reply to this post"
msgstr "Respon a esta publicació"

#: content/ui/StatusComposer/StatusComposer.qml:49
#, kde-format
msgid "Rewrite this post"
msgstr "Torna a escriure esta publicació"

#: content/ui/StatusComposer/StatusComposer.qml:51
#, kde-format
msgid "Write a new post"
msgstr "Escriviu una publicació nova"

#: content/ui/StatusComposer/StatusComposer.qml:93
#: content/ui/StatusComposer/StatusComposer.qml:277
#, kde-format
msgid "Content Warning"
msgstr "Avís de contingut"

#: content/ui/StatusComposer/StatusComposer.qml:109
#, kde-format
msgid "What's new?"
msgstr "Què hi ha de nou?"

#: content/ui/StatusComposer/StatusComposer.qml:211
#, kde-format
msgid "Attach File"
msgstr "Adjunta un fitxer"

#: content/ui/StatusComposer/StatusComposer.qml:221
#, kde-format
msgid "Add Poll"
msgstr "Afig una votació"

#: content/ui/StatusComposer/StatusComposer.qml:249
#: content/ui/StatusDelegate/StatusDelegate.qml:447
#: content/ui/StatusDelegate/StatusDelegate.qml:455
#, kde-format
msgid "Public"
msgstr "Públic"

#: content/ui/StatusComposer/StatusComposer.qml:254
#: content/ui/StatusDelegate/StatusDelegate.qml:449
#, kde-format
msgid "Unlisted"
msgstr "No llistat"

#: content/ui/StatusComposer/StatusComposer.qml:259
#: content/ui/StatusDelegate/StatusDelegate.qml:451
#, kde-format
msgid "Private"
msgstr "Privat"

#: content/ui/StatusComposer/StatusComposer.qml:264
#: content/ui/StatusDelegate/StatusDelegate.qml:453
#, kde-format
msgid "Direct Message"
msgstr "Missatge directe"

#: content/ui/StatusComposer/StatusComposer.qml:269
#: content/ui/StatusDelegate/StatusDelegate.qml:443
#, kde-format
msgid "Visibility"
msgstr "Visibilitat"

#: content/ui/StatusComposer/StatusComposer.qml:274
#, kde-format
msgctxt "Short for content warning"
msgid "cw"
msgstr "ac"

#: content/ui/StatusComposer/StatusComposer.qml:284
#, kde-format
msgctxt "@info:whatsthis Post language selection"
msgid "Select the language the post is written in"
msgstr "Seleccioneu l'idioma en què s'ha escrit la publicació"

#: content/ui/StatusComposer/StatusComposer.qml:290
#, kde-format
msgctxt "@label:listbox Post language selection"
msgid "Post Language"
msgstr "Idioma de la publicació"

#: content/ui/StatusComposer/StatusComposer.qml:305
#, kde-format
msgctxt "@label Character count in the status composer"
msgid "<b>%1/%2</b> characters"
msgstr "<b>%1/%2</b> caràcters"

#: content/ui/StatusComposer/StatusComposer.qml:313
#, kde-format
msgid "Send"
msgstr "Envia"

#: content/ui/StatusDelegate/AttachmentGrid.qml:118
#, kde-format
msgid "Not available"
msgstr "No disponible"

#: content/ui/StatusDelegate/AttachmentGrid.qml:213
#, kde-format
msgid "Media Hidden"
msgstr "Fitxers multimèdia ocults"

#: content/ui/StatusDelegate/InlineIdentityInfo.qml:25
#: content/ui/StatusDelegate/StatusDelegate.qml:150
#: content/ui/StatusDelegate/StatusDelegate.qml:250
#, kde-format
msgid "View profile"
msgstr "Visualitza el perfil"

#: content/ui/StatusDelegate/OverflowMenu.qml:21
#, kde-format
msgid "Expand This Post"
msgstr "Expandix esta publicació"

#: content/ui/StatusDelegate/OverflowMenu.qml:28
#, kde-format
msgid "Open Original Page"
msgstr "Obri la pàgina original"

#: content/ui/StatusDelegate/OverflowMenu.qml:36
#, kde-format
msgid "Copy Link to This Post"
msgstr "Copia l'enllaç a esta publicació"

#: content/ui/StatusDelegate/OverflowMenu.qml:39
#, kde-format
msgid "Post link copied."
msgstr "S'ha copiat l'enllaç de la publicació."

#: content/ui/StatusDelegate/OverflowMenu.qml:47
#: content/ui/StatusDelegate/StatusDelegate.qml:399
#, kde-format
msgid "Remove bookmark"
msgstr "Elimina el punt"

#: content/ui/StatusDelegate/OverflowMenu.qml:47
#, kde-format
msgid "Bookmark"
msgstr "Punt"

#: content/ui/StatusDelegate/OverflowMenu.qml:72
#, kde-format
msgid "Delete & Re-draft"
msgstr "Suprimix i torna a esborrany"

#: content/ui/StatusDelegate/Poll.qml:39
#, kde-format
msgctxt "Votes percentage"
msgid "%1%"
msgstr "%1%"

#: content/ui/StatusDelegate/Poll.qml:63
#, kde-format
msgid "(No votes)"
msgstr "(Sense vots)"

#: content/ui/StatusDelegate/Poll.qml:97
#, kde-format
msgid "Vote"
msgstr "Voteu"

#: content/ui/StatusDelegate/StatusDelegate.qml:113
#, kde-format
msgid "Filtered: %1"
msgstr "Filtrades: %1"

#: content/ui/StatusDelegate/StatusDelegate.qml:117
#, kde-format
msgid "Show anyway"
msgstr "Mostra igualment"

#: content/ui/StatusDelegate/StatusDelegate.qml:164
#, kde-format
msgid "Your poll has ended"
msgstr "La votació ha finalitzat"

#: content/ui/StatusDelegate/StatusDelegate.qml:166
#, kde-format
msgid "A poll you voted in has ended"
msgstr "La votació en la qual heu votat, ha finalitzat"

#: content/ui/StatusDelegate/StatusDelegate.qml:191
#, kde-format
msgid "Pinned entry"
msgstr "Entrada fixada"

#: content/ui/StatusDelegate/StatusDelegate.qml:258
#, kde-format
msgid "%1 boosted"
msgstr "%1 ha impulsat"

#: content/ui/StatusDelegate/StatusDelegate.qml:260
#, kde-format
msgid "In reply to %1"
msgstr "En resposta a %1"

#: content/ui/StatusDelegate/StatusDelegate.qml:260
#, kde-format
msgid "%1 replied to your post"
msgstr "%1 ha respost la vostra publicació"

#: content/ui/StatusDelegate/StatusDelegate.qml:303
#, kde-format
msgid "Show Less"
msgstr "Mostra'n menys"

#: content/ui/StatusDelegate/StatusDelegate.qml:303
#, kde-format
msgid "Show More"
msgstr "Mostra'n més"

#: content/ui/StatusDelegate/StatusDelegate.qml:357
#, kde-format
msgctxt "Reply to a post"
msgid "Reply"
msgstr "Respon"

#: content/ui/StatusDelegate/StatusDelegate.qml:358
#, kde-format
msgctxt "More than one reply"
msgid "1+"
msgstr "1+"

#: content/ui/StatusDelegate/StatusDelegate.qml:372
#, kde-format
msgctxt "Share a post"
msgid "Boost"
msgstr "Interessant"

#: content/ui/StatusDelegate/StatusDelegate.qml:386
#, kde-format
msgctxt "Like a post"
msgid "Like"
msgstr "M'agrada"

#: content/ui/StatusDelegate/StatusDelegate.qml:399
#, kde-format
msgctxt "Bookmark a post"
msgid "Bookmark"
msgstr "Afig un punt"

#: content/ui/StatusDelegate/StatusDelegate.qml:411
#, kde-format
msgctxt "Show more options"
msgid "More"
msgstr "Més"

#: content/ui/StatusDelegate/StatusDelegate.qml:472
#, kde-format
msgid "via %1"
msgstr "mitjançant %1"

#: content/ui/TimelinePage.qml:110
#, kde-format
msgid "No posts"
msgstr "Sense publicacions"

#: content/ui/UserInfo.qml:72
#, kde-format
msgid "Log in to an existing account"
msgstr "Inicia la sessió amb un compte existent"

#: content/ui/UserInfo.qml:195
#, kde-format
msgid "Switch user"
msgstr "Canvia d'usuari"

#: content/ui/UserInfo.qml:207
#, kde-format
msgid "Add"
msgstr "Afig"

#: content/ui/UserInfo.qml:219
#, kde-format
msgid "Open settings"
msgstr "Obri la configuració"

#: conversation/conversationmodel.cpp:59
#, kde-format
msgid "Empty conversation"
msgstr "Conversa buida"

#: conversation/conversationmodel.cpp:63
#, kde-format
msgid "%1 and %2"
msgstr "%1 i %2"

#: conversation/conversationmodel.cpp:65
#, kde-format
msgid "%2 and one other"
msgid_plural "%2 and %1 others"
msgstr[0] "%2 i un altre"
msgstr[1] "%2 i %1 altres"

#: editor/polltimemodel.cpp:12
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "5 minutes"
msgstr "5 minuts"

#: editor/polltimemodel.cpp:13
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "30 minutes"
msgstr "30 minuts"

#: editor/polltimemodel.cpp:14
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "1 hour"
msgstr "1 hora"

#: editor/polltimemodel.cpp:15
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "6 hours"
msgstr "6 hores"

#: editor/polltimemodel.cpp:16
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "12 hours"
msgstr "12 hores"

#: editor/polltimemodel.cpp:17
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "1 day"
msgstr "1 dia"

#: editor/polltimemodel.cpp:18
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "3 days"
msgstr "3 dies"

#: editor/polltimemodel.cpp:19
#, kde-format
msgctxt "@item:inlistbox Poll expire times"
msgid "7 days"
msgstr "7 dies"

#: editor/posteditorbackend.cpp:240
#, kde-format
msgid "An unknown error occurred."
msgstr "S'ha produït un error desconegut."

#: main.cpp:112
#, kde-format
msgid "Tokodon"
msgstr "Tokodon"

#: main.cpp:114
#, kde-format
msgid "Mastodon client"
msgstr "Client de Mastodon"

#: main.cpp:116
#, kde-format
msgid "© 2021-2023 Carl Schwan, 2021-2023 KDE Community"
msgstr "© 2021-2023 Carl Schwan, 2021-2023 Comunitat KDE"

#: main.cpp:117
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:117
#, kde-format
msgid "Maintainer"
msgstr "Mantenidor"

#: main.cpp:118
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#: main.cpp:118
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: main.cpp:126
#, kde-format
msgid "Client for the decentralized social network: mastodon"
msgstr "Client per a la xarxa social descentralitzada: Mastodon"

#: main.cpp:127
#, kde-format
msgid "Supports web+ap: url scheme"
msgstr "Implementa l'esquema «web+ap: url»"

#: search/searchmodel.cpp:137
#, kde-format
msgid "People"
msgstr "Persones"

#: search/searchmodel.cpp:139
#, kde-format
msgid "Hashtags"
msgstr "Etiqueta"

#: timeline/accountmodel.cpp:34 timeline/maintimelinemodel.cpp:27
#, kde-format
msgid "Loading"
msgstr "S'està carregant"

#: timeline/linkpaginatedtimelinemodel.cpp:25 timeline/maintimelinemodel.cpp:38
#, kde-format
msgctxt "@title"
msgid "Bookmarks"
msgstr "Punts"

#: timeline/linkpaginatedtimelinemodel.cpp:27 timeline/maintimelinemodel.cpp:40
#, kde-format
msgctxt "@title"
msgid "Favourites"
msgstr "Preferits"

#: timeline/linkpaginatedtimelinemodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Trending"
msgstr "Tendències"

#: timeline/maintimelinemodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Home (%1)"
msgstr "Inici (%1)"

#: timeline/maintimelinemodel.cpp:31
#, kde-format
msgctxt "@title"
msgid "Home"
msgstr "Inici"

#: timeline/maintimelinemodel.cpp:34
#, kde-format
msgctxt "@title"
msgid "Local Timeline"
msgstr "Línia de temps local"

#: timeline/maintimelinemodel.cpp:36
#, kde-format
msgctxt "@title"
msgid "Global Timeline"
msgstr "Línia de temps global"

#: timeline/notificationmodel.cpp:99
#, kde-format
msgid "Error occurred when fetching the latest notification."
msgstr "S'ha produït un error en recuperar l'última notificació."

#: timeline/post.cpp:309
#, kde-format
msgid "in the future"
msgstr "En el futur"

#: timeline/post.cpp:311
#, kde-format
msgid "%1s"
msgstr "%1s"

#: timeline/post.cpp:313
#, kde-format
msgid "%1m"
msgstr "%1m"

#: timeline/post.cpp:315
#, kde-format
msgid "%1h"
msgstr "%1h"

#: timeline/post.cpp:317
#, kde-format
msgid "%1d"
msgstr "%1d"

#: timeline/post.cpp:321
#, kde-format
msgid "1 week ago"
msgstr "Fa 1 setmana"

#: timeline/post.cpp:323
#, kde-format
msgid "%1 weeks ago"
msgstr "Fa %1 setmanes"

#: timeline/post.cpp:327
#, kde-format
msgid "1 month ago"
msgstr "Fa 1 mes"

#: timeline/post.cpp:329
#, kde-format
msgid "%1 months ago"
msgstr "Fa %1 mesos"

#: timeline/post.cpp:335
#, kde-format
msgid "1 year ago"
msgstr "Fa 1 any"

#: timeline/post.cpp:337
#, kde-format
msgid "%1 years ago"
msgstr "Fa %1 anys"

#: timeline/threadmodel.cpp:29
#, kde-format
msgctxt "@title"
msgid "Thread"
msgstr "Fil"

#: utils/filetransferjob.cpp:82
#, kde-format
msgctxt "Job heading, like 'Copying'"
msgid "Downloading"
msgstr "S'està baixant"

#: utils/filetransferjob.cpp:83
#, kde-format
msgctxt "The URL being downloaded/uploaded"
msgid "Source"
msgstr "Font"

#: utils/filetransferjob.cpp:84
#, kde-format
msgctxt "The location being downloaded to"
msgid "Destination"
msgstr "Destinació"

#~ msgid "Toot"
#~ msgstr "Tut"
